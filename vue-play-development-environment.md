# ディレクトリ構成(開始時点)
- example-app
    - front
        - Dockerfile
    - server
        - Dockerfile
    - docker-compose.yml

# Database(MySQL)
後述のdocker-compose化で作成。

# Front(Vue)
## Imageのビルド

```sh
$ cd front
$ docker build -t vue:20200110 .
```

## コンテナの作成

```sh
# -vオプションでホストのディレクトリをコンテナへマウント
#   -v $(pwd):/app => 現在いるディレクトリをコンテナの/appへマウント
# -pでポートフォワードの指定、複数指定可能
#   -p 8880:8080 => ホストから8880ポートにアクセスすると、コンテナの8080ポートへ飛ばす
# --nameでコンテナの名前を指定、指定しない場合は自動で何かしらの名前がつく
$ docker run -v $(pwd):/app -p 8880:8080 --name example-app-front -it -d vue:20200110
$ docker ps
CONTAINER ID        IMAGE               COMMAND                  CREATED              STATUS              PORTS                    NAMES
b677f179162a        node:20200113       "docker-entrypoint.s…"   About a minute ago   Up About a minute   0.0.0.0:8880->8080/tcp   example-app-front
```

## 作成したコンテナへ繋ぐ
※ `docker exec`自体はコンテナでコマンドを実行するために使用するオプションだが、ここでシェル(ash)
を指定することで、コンテナ内に入りコマンドを実行することができる

```sh
$ docker exec -it example-app-front ash
/app #
```

※ 以降はコンテナ内で実行

## 各コマンドのバージョンを確認

```sh
$ npm -v
6.13.6
$ yarn -v
1.21.1
$ vue -V
@vue/cli 4.1.2
```

## プロジェクトの作成

```sh
$ pwd
/app
$ vue create .
# 以下が出たらY
?  Your connection to the default yarn registry seems to be slow.
   Use https://registry.npm.taobao.org for faster installation? (Y/n) Y

# 現在いるディレクトリに作ってよいか？
Vue CLI v4.1.2
> Generate project in current directory? (Y/n) Y

# 使用する構成の選択、今回はdefaultを使用
Vue CLI v4.1.2
? Please pick a preset:
> default (babel, eslint)
  Manually select features

# パッケージ管理に使用するマネージャーの選択、今回はYarnを使用
Vue CLI v4.1.2
? Please pick a preset: default (babel, eslint)
? Pick the package manager to use when installing dependencies: (Use arrow keys)
> Use Yarn
  Use NPM

# Done.が出たら完了
yarn install v1.21.1
[1/4] Resolving packages...
[2/4] Fetching packages...
info fsevents@1.2.11: The platform "linux" is incompatible with this module.
info "fsevents@1.2.11" is an optional dependency and failed compatibility check. Excluding it from installation.

success Saved lockfile.
Done in 8.02s.
```

## 起動してみる

```sh
$ yarn serve
```

起動後、ブラウザで `localhost:8880` にアクセスし、"Welcome to Your Vue.js App"のサンプルページが表示されればOK

# Server(Play)

## Imageのビルド

```sh
$ cd server
$ docker build -t sbt:20200110 .
```

## コンテナの作成

```sh
$ docker run -v $(pwd):/app -p 9900:9000 --name example-app-server -it -d sbt:20200110
```

## 作成したコンテナへ繋ぐ

```sh
$ docker exec -it example-app-server ash
```

## Playのプロジェクトを作成

```sh
$ sbt new playframework/play-java-seed.g8
name [play-java-seed]: example-app
organization [com.example]: com.exampleapp
```

## 起動してみる

```sh
$ cd example-app
$ sbt run
```

起動後、ブラウザで `localhost:9900` にアクセスし、"Welcome to Play"のサンプルが表示されればOK